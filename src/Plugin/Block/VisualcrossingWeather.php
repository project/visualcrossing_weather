<?php

namespace Drupal\visualcrossing_weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'Visual Crossing Weather Forecast' Block.
 *
 * @Block(
 *   id = "visualcrossing_weather",
 *   admin_label = @Translation("Visual Crossing Weather Forecast"),
 * )
 */
class VisualcrossingWeather extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CacheBackendInterface $cache_backend, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cacheBackend = $cache_backend;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('visualcrossing_weather.visualcrossing_cache'), $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => 0,
	  'api_key' => '',
      'location' => 'London, UK',
	  'mode' => 'simple',
	  'forecast_days' => 7,
	  'unit' => 'metric',
      'title' => '',
	  'show_title' => false,
	  'show_condition' => false,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['weather'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Visual Crossing Weather Forecast settings'),
    ];
	
	$form['weather']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['api_key'],
    ];

    $form['weather']['location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['location'],
    ];
	
	$form['weather']['mode'] = [
      '#type' => 'select',
      '#options' => array("simple"=>"Simple", "d3"=>"D3"),
      '#title' => $this->t('Mode'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['mode'],
    ];
	
	$form['weather']['forecast_days'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Forecast Days'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['forecast_days'],
    ];
	
	$form['weather']['unit'] = [
      '#type' => 'select',
      '#options' => array("metric"=>"Metric", "us"=>"US"),
      '#title' => $this->t('Units'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['unit'],
    ];

    $form['weather']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['title'],
    ];
	
	$form['weather']['show_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Title'),
      '#default_value' => $this->configuration['show_title'],
    ];
	
	$form['weather']['show_condition'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Condition'),
      '#default_value' => $this->configuration['show_condition'],
    ];

    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $block_values = $form_state->getValue('weather');
    $this->configuration['location'] = $block_values['location'];
    $this->configuration['api_key'] = $block_values['api_key'];
    $this->configuration['mode'] = $block_values['mode'];
    $this->configuration['forecast_days'] = $block_values['forecast_days'];
    $this->configuration['unit'] = $block_values['unit'];
    $this->configuration['title'] = $block_values['title'];
	$this->configuration['show_title'] = $block_values['show_title'];
	$this->configuration['show_condition'] = $block_values['show_condition'];
	$this->configuration['widget_id'] = uniqid();

    Cache::invalidateTags($this->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'visualcrossing_weather',
      '#wconfig' => $this->get_settings_values(),
      '#attached' => [
        'library' => 'visualcrossing_weather/drupal.weather',
      ],
      '#cache' => [
        'tags' => $this->getCacheTags(),
      ],
    ];
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(
        parent::getCacheTags(), ['visualcrossing_weather:block']
    );
  }

  public function get_settings_values(){
	  
	  $wconfig = $this->configuration;
	  if(!isset($wconfig['location'])){
		$wconfig = $this->defaultConfiguration();  
	  }
	  return $wconfig;
  }

}
