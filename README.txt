INTRODUCTION
------------

This module provides a weather block using visualcrossing.com API.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

Once the module has been installed, navigate to 
admin/structure/block
(Structure > Block Layout through the administration panel)
and add and configure the new block.

REQUIREMENTS
-----------

* Drupal core block module

MAINTAINERS
-----------

Current maintainers:
 * Codextent (codextent)
 - https://www.drupal.org/u/codextent
